# Do not run this script except on a VM.
# First we just check to make sure some commands we'll need are already installed.
# curl, we will install (and configure).
whoami
usermod --help
echo "install-docker: fix_all_gotchas.sh 1"
. ./fix_all_gotchas.sh
echo "install-docker: apt-get install curl"
# ubuntu:devel does not have sudo.
apt-get update || sudo apt-get update
apt-get install --assume-yes curl || sudo apt-get install --assume-yes curl
# add-apt-repository is contained in software-properties-common.
apt-get install --assume-yes software-properties-common || sudo apt-get install --assume-yes software-properties-common
apt-get install --assume-yes ca-certificates || sudo apt-get install --assume-yes ca-certificates
# apt-get install --assume-yes gnupg || sudo apt-get install --assume-yes gnupg
# if ! [ -z ${PROXY_CA_PEM+ABC} ]; then apt-key add "$PROXY_CA_PEM"; fi
# Now that curl is installed, re-run fix_all_gotchas.sh to configure curl.
echo "install-docker: fix_all_gotchas.sh 2"
. ./fix_all_gotchas.sh
echo "install-docker: curl get.docker.com"
# curl --show-error --location https://raw.githubusercontent.com/dHannasch/docker-install/use-add-apt-repository/install.sh --output get-docker.sh
curl --show-error --location https://get.docker.com --output get-docker.sh
echo "install-docker: sh get-docker.sh"
sh get-docker.sh
usermod --groups docker --append $(whoami)
